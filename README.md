# Birdie Developer Test

## Dependencies

- nodejs

## Deployment

Deployment to heroku is managed by gitlab ci, see .gitlab-ci.yml

## Frontend

React front end developed using TypeScript, React, Redux-Toolkit

### Setup & Installation

- set environment variable REACT_APP_API_BASE_URL to the base url of your backend instance (see /frontend/.env.example)
- naviagate to /frontend and run `npm install`

### Usage

- to start a development environment naviagate to /frontend and run `npm start`
- to compile production code naviagate to /frontend and run `npm run build`

### Testing

this repo makes use of Jest + Enzyme for unit testing,

- tests can be executed by naviagating to /frontend and runing `npm run test`

## Backend

Express Typescript backend

### Setup & Intallation

- set environment variables for database connection (see /backend/.env.example)
- naviagate to /backend and run `npm install`

### Usage

- to start a development environment naviagate to /backend and run `npm start`
- to compile production code naviagate to /backend and run `npm run build`
- this will produce compliled JavaScript code, the server can be started by executing index.js

### Testing

Uses Jest to perform snapshot tests on endpoints, when running tests we need to connect to the test data to ensure we are comparing the same data when performing our snapshot tests.

- tests can be executed by naviagating to /backend and runing `npm run test`
