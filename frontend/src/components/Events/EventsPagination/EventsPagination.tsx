import React, { ReactElement } from 'react';
import { useAppDispatch } from '../../../hooks';
import { changePage, changeRowCount, fetchEvents } from '../EventsSlice';

import './EventsPagination.css';

type EventsPaginationProps = {
	currentPage: number;
	totalPages: number;
	totalRecords: number;
	rowCount: number;
};

export const EventsPagination = ({
	currentPage,
	totalPages,
	totalRecords,
	rowCount,
}: EventsPaginationProps): ReactElement => {
	let dispatch = useAppDispatch();

	const handleNextPage = () => {
		if (currentPage + 1 < totalPages) {
			dispatch(changePage(currentPage + 1));
			dispatch(fetchEvents());
		}
	};

	const handlePrevPage = () => {
		if (currentPage !== 0) {
			dispatch(changePage(currentPage - 1));
			dispatch(fetchEvents());
		}
	};

	const handleChangeRowCount = (payload: string) => {
		if (parseInt(payload, 10)) {
			dispatch(changeRowCount(parseInt(payload, 10)));
			dispatch(fetchEvents());
		}
	};

	return (
		<div className="events-pagination">
			<div className="events-pagination-controls">
				<div
					className="events-pagination-arrow"
					onClick={() => handlePrevPage()}
				>
					<i className="fas fa-arrow-circle-left"></i>
				</div>
				<div className="events-pagination-counts">{`Page ${
					currentPage + 1
				} of ${totalPages}`}</div>
				<div
					className="events-pagination-arrow"
					onClick={() => handleNextPage()}
				>
					<i className="fas fa-arrow-circle-right"></i>
				</div>
			</div>
			<div className="events-pagination-records">
				<input
					type="number"
					value={rowCount}
					onChange={(e) => handleChangeRowCount(e.currentTarget.value)}
				></input>
				<div>Events per page</div>
				<div>{`${totalRecords} Events`}</div>
			</div>
		</div>
	);
};
