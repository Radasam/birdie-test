import React from 'react';
import { mount } from 'enzyme';
import { EventsPagination } from './EventsPagination';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { EnhancedStore } from '@reduxjs/toolkit';
import configureStore from 'redux-mock-store';
import { changePage, changeRowCount } from '../EventsSlice';

const mockStore = configureStore([]);

describe('EventsFilter', function () {
	let store: EnhancedStore;

	beforeEach(() => {
		store = mockStore({});

		store.dispatch = jest.fn();
	});

	it('should render without throwing an error', function () {
		let component = mount(
			<Provider store={store}>
				<EventsPagination
					currentPage={0}
					totalPages={2}
					totalRecords={50}
					rowCount={25}
				/>
			</Provider>
		);
		expect(component.find('.events-pagination-counts').text()).toBe(
			`Page 1 of 2`
		);
	});

	it('should move page on click', function () {
		let currentPage = 0;
		let component = mount(
			<Provider store={store}>
				<EventsPagination
					currentPage={currentPage}
					totalPages={2}
					totalRecords={50}
					rowCount={25}
				/>
			</Provider>
		);
		component.find('.events-pagination-arrow').at(1).simulate('click');

		expect(store.dispatch).toHaveBeenCalledWith(changePage(currentPage + 1));
	});

	it('should move page on click', function () {
		let currentPage = 1;
		let component = mount(
			<Provider store={store}>
				<EventsPagination
					currentPage={currentPage}
					totalPages={2}
					totalRecords={50}
					rowCount={25}
				/>
			</Provider>
		);
		component.find('.events-pagination-arrow').at(0).simulate('click');

		expect(store.dispatch).toHaveBeenCalledWith(changePage(0));
	});

	it('should not move page if on first page', function () {
		let currentPage = 0;
		let component = mount(
			<Provider store={store}>
				<EventsPagination
					currentPage={currentPage}
					totalPages={0}
					totalRecords={50}
					rowCount={25}
				/>
			</Provider>
		);
		component.find('.events-pagination-arrow').at(0).simulate('click');

		expect(store.dispatch).toHaveBeenCalledTimes(0);
	});

	it('should not move page if on last page', function () {
		let currentPage = 2;
		let component = mount(
			<Provider store={store}>
				<EventsPagination
					currentPage={currentPage}
					totalPages={2}
					totalRecords={50}
					rowCount={25}
				/>
			</Provider>
		);
		component.find('.events-pagination-arrow').at(1).simulate('click');

		expect(store.dispatch).toHaveBeenCalledTimes(0);
	});

	it('change row count', function () {
		let currentPage = 0;
		let component = mount(
			<Provider store={store}>
				<EventsPagination
					currentPage={currentPage}
					totalPages={2}
					totalRecords={50}
					rowCount={25}
				/>
			</Provider>
		);

		let input = component.find('input');
		let inputDom = input.getDOMNode() as HTMLInputElement;
		inputDom.value = '50';
		input.simulate('change', { currentTarget: inputDom });

		expect(store.dispatch).toHaveBeenCalledWith(changeRowCount(50));
	});
});
