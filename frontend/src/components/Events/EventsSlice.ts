import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../../store';
import {
	getCaregivers,
	getEvents,
	getEventsRefData,
	getRecipients,
} from './EventsAPI';
import {
	CaregiversResponse,
	EventsResponse,
	EventType,
	EventTypesResponse,
	RecipientsResponse,
	User,
} from './EventsTypes';

// Define a type for state
interface EventsState {
	eventTypes: string[];
	selectedTypes: string[];
	selectedCareGiver: string;
	selectedRecipient: string;
	events: Array<EventType>;
	recipients: User[];
	caregivers: User[];
	filterMinDate: string;
	filterMaxDate: string;
	refDataLoaded: boolean;
	eventsLoaded: boolean;
	currentPage: number;
	rowCount: number;
	totalPages: number;
	totalRecords: number;
}

// Define initial state
export const initialState: EventsState = {
	eventTypes: [],
	selectedTypes: [],
	selectedCareGiver: '',
	selectedRecipient: '',
	events: [],
	recipients: [],
	caregivers: [],
	filterMinDate: '',
	filterMaxDate: '',
	refDataLoaded: false,
	eventsLoaded: false,
	currentPage: 0,
	rowCount: 25,
	totalPages: 0,
	totalRecords: 0,
};

export const fetchEventsRefData = createAsyncThunk<EventTypesResponse>(
	'events/fetchRefData',
	async () => {
		return await getEventsRefData();
	}
);

export const fetchEvents = createAsyncThunk<
	EventsResponse,
	void,
	{ state: RootState }
>('events/fetchEvents', async (_, { getState }) => {
	let state = getState();
	return (await getEvents(
		state.events.selectedTypes,
		state.events.selectedCareGiver,
		state.events.selectedRecipient,
		state.events.filterMinDate,
		state.events.filterMaxDate,
		state.events.currentPage,
		state.events.rowCount
	)) as EventsResponse;
});

export const fetchRecipients = createAsyncThunk<RecipientsResponse>(
	'events/fetchRecipients',
	async () => {
		return await getRecipients();
	}
);

export const fetchCaregivers = createAsyncThunk<CaregiversResponse>(
	'events/fetchCaregivers',
	async () => {
		return await getCaregivers();
	}
);

export const eventsSlice = createSlice({
	name: 'events',
	initialState,
	reducers: {
		selectType: (state, action: PayloadAction<string>) => {
			state.selectedTypes = state.selectedTypes.concat([action.payload]);
			state.currentPage = 0;
		},
		removeType: (state, action: PayloadAction<string>) => {
			state.selectedTypes = state.selectedTypes.filter(
				(type) => type !== action.payload
			);
			state.currentPage = 0;
		},
		changeCaregiver: (state, action: PayloadAction<string>) => {
			state.selectedCareGiver = action.payload;
			state.currentPage = 0;
		},
		changeRecipient: (state, action: PayloadAction<string>) => {
			state.selectedRecipient = action.payload;
			state.currentPage = 0;
		},
		changeDates: (
			state,
			action: PayloadAction<{ startDate: string; endDate: string }>
		) => {
			state.filterMinDate = action.payload.startDate;
			state.filterMaxDate = action.payload.endDate;
			state.currentPage = 0;
		},
		changePage: (state, action: PayloadAction<number>) => {
			state.currentPage = action.payload;
		},
		changeRowCount: (state, action: PayloadAction<number>) => {
			state.rowCount = action.payload;
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchEventsRefData.pending, (state, { payload }) => {
			state.refDataLoaded = false;
		});
		builder.addCase(fetchEventsRefData.fulfilled, (state, { payload }) => {
			state.eventTypes = payload.event_types;
			state.filterMinDate = payload.start_date;
			state.filterMaxDate = payload.end_date;
			state.refDataLoaded = true;
		});
		builder.addCase(fetchEvents.pending, (state, { payload }) => {
			state.eventsLoaded = false;
		});
		builder.addCase(fetchEvents.fulfilled, (state, { payload }) => {
			state.events = payload.events;
			state.totalPages = payload.pages;
			state.totalRecords = payload.records;
			state.eventsLoaded = true;
		});
		builder.addCase(fetchRecipients.fulfilled, (state, { payload }) => {
			state.recipients = payload.recipients;
		});
		builder.addCase(fetchCaregivers.fulfilled, (state, { payload }) => {
			state.caregivers = payload.caregivers;
		});
	},
});

export const {
	selectType,
	removeType,
	changeCaregiver,
	changeRecipient,
	changeDates,
	changePage,
	changeRowCount,
} = eventsSlice.actions;

export default eventsSlice.reducer;
