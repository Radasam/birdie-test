import moment, { Moment } from 'moment';
import React, { ReactElement, useState } from 'react';
import { DateRangePicker, FocusedInputShape } from 'react-dates';
import { useAppDispatch } from '../../../hooks';
import { changeDates, fetchEvents } from '../EventsSlice';

import './EventsDatePicker.css';

type EventsDatePickerType = {
	filterMinDate: string;
	filterMaxDate: string;
};

export const EventsDatePicker = ({
	filterMinDate,
	filterMaxDate,
}: EventsDatePickerType): ReactElement => {
	const [dateRangeFocus, setDateRangeFocus] =
		useState<FocusedInputShape | null>(null);

	const dispatch = useAppDispatch();

	const handleDateChange = (payload: {
		startDate: Moment | null;
		endDate: Moment | null;
	}) => {
		if (payload.startDate && payload.endDate) {
			dispatch(
				changeDates({
					startDate: payload.startDate.format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
					endDate: payload.endDate.format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
				})
			);
			dispatch(fetchEvents());
		}
	};

	return (
		<div className="events-date">
			<h3>Dates:</h3>
			<div className="events-date-picker">
				<DateRangePicker
					startDate={moment(filterMinDate)}
					startDateId={'events-filter-start-date'}
					endDate={moment(filterMaxDate)}
					endDateId={'events-filter-end-date'}
					onDatesChange={({
						startDate,
						endDate,
					}: {
						startDate: Moment | null;
						endDate: Moment | null;
					}) => {
						handleDateChange({ startDate, endDate });
					}}
					focusedInput={dateRangeFocus}
					onFocusChange={(focusedInput) => setDateRangeFocus(focusedInput)}
					isOutsideRange={() => false}
					enableOutsideDays={false}
				></DateRangePicker>
			</div>
		</div>
	);
};
