import React from 'react';
import { mount } from 'enzyme';
import { EventsDatePicker } from './EventsDatePicker';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { DateRangePicker } from 'react-dates';

import 'react-dates/initialize';

describe('EventsDatePicker', function () {
	it('should render without throwing an error', function () {
		let filterMinDate = '2019-04-23T02:00:11.000Z';
		let filterMaxDate = '2019-05-12T22:06:34.000Z';
		let component = mount(
			<Provider store={store}>
				<EventsDatePicker
					filterMinDate={filterMinDate}
					filterMaxDate={filterMaxDate}
				/>
			</Provider>
		);
		expect(component.find(DateRangePicker)).toHaveLength(1);
	});
});
