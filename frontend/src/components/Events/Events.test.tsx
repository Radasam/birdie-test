import React from 'react';
import { mount } from 'enzyme';
import { Events } from './Events';
import { Provider } from 'react-redux';
import { store } from '../../store';
import { EventType, User } from './EventsTypes';
import { EventsDatePicker } from './EventsDatePicker/EventsDatePicker';
import { EventsDropDown } from './EventsDropDown/EventsDropDown';
import { EventsFilters } from './EventsFilters/EventsFilters';
import { EventsTable } from './EventsTable/EventsTable';

describe('Events', function () {
	it('should render without throwing an error', function () {
		let eventTypes = ['test-1', 'test-2'];
		let caregivers: User[] = [
			{ id: 'test-user-1', name: 'test-user-1' },
			{ id: 'test-user-2', name: 'test-user-2' },
		];
		let recipients: User[] = [
			{ id: 'test-user-3', name: 'test-user-3' },
			{ id: 'test-user-4', name: 'test-user-4' },
		];
		let events: EventType[] = [
			{
				id: 'abc-1',
				visit_id: 'def-1',
				timestamp: '2021-07-23T00:00:00.000Z',
				event_type: 'fluid_intake_observation',
				caregiver_id: 'ghi-1',
				care_recipient_id: 'jkl-1',
				caregiver_name: 'test-caregiver',
				care_recipient_name: 'test-recipient',
			},
			{
				id: 'abc-2',
				visit_id: 'def-2',
				timestamp: '2021-07-20T00:00:00.000Z',
				event_type: 'fluid_intake_observation',
				caregiver_id: 'ghi-2',
				care_recipient_id: 'jkl-2',
				caregiver_name: 'test-caregiver-2',
				care_recipient_name: 'test-recipient-2',
				task_definition_description: 'test-desc',
			},
		];
		let filterMinDate = '2019-04-23T02:00:11.000Z';
		let filterMaxDate = '2019-05-12T22:06:34.000Z';
		let component = mount(
			<Provider store={store}>
				<Events
					eventTypes={eventTypes}
					selectedTypes={['test-1']}
					caregivers={caregivers}
					recipients={recipients}
					events={events}
					filterMinDate={filterMinDate}
					filterMaxDate={filterMaxDate}
					refDataLoaded={true}
					eventsLoaded={true}
					totalPages={1}
					currentPage={0}
					totalRecords={events.length}
					rowCount={25}
				/>
			</Provider>
		);
		expect(component.find(EventsDatePicker)).toHaveLength(1);
		expect(component.find(EventsDropDown)).toHaveLength(2);
		expect(component.find(EventsFilters)).toHaveLength(1);
		expect(component.find(EventsTable)).toHaveLength(1);
	});
});
