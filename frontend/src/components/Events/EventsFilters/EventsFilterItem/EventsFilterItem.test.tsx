import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { store } from '../../../../store';
import { removeType, selectType } from '../../EventsSlice';
import { EventsFilterItem } from './EventsFilterItem';
import { EnhancedStore } from '@reduxjs/toolkit';
import configureStore from 'redux-mock-store';

const mockStore = configureStore([]);

describe('EventsFilterItem', function () {
	let store: EnhancedStore;

	beforeEach(() => {
		store = mockStore({});

		store.dispatch = jest.fn();
	});

	it('should render without throwing an error', function () {
		store = mockStore({
			events: {},
		});
		let component = mount(
			<Provider store={store}>
				<EventsFilterItem eventType="test-event" selected={true} />
			</Provider>
		);
		expect(component.find('.events-filter-item')).toHaveLength(1);
		expect(
			component
				.find('.events-filter-item')
				.children('div')
				.at(0)
				.hasClass('filter-item-background')
		).toBeTruthy();
	});

	it('should render selected filter item', function () {
		store = mockStore({
			events: {},
		});
		let component = mount(
			<Provider store={store}>
				<EventsFilterItem eventType="test-event" selected={true} />
			</Provider>
		);
		expect(component.find('.events-filter-item')).toHaveLength(1);
		expect(
			component
				.find('.events-filter-item')
				.children('div')
				.at(0)
				.hasClass('filter-item-background --selected')
		).toBeTruthy();
	});

	it('click should select filter', function () {
		let component = mount(
			<Provider store={store}>
				<EventsFilterItem eventType="test-event" selected={false} />
			</Provider>
		);
		component.find('.events-filter-item').simulate('click');

		expect(store.dispatch).toHaveBeenCalledWith(selectType('test-event'));
	});

	it('click should deselect filter', function () {
		let component = mount(
			<Provider store={store}>
				<EventsFilterItem eventType="test-event" selected={true} />
			</Provider>
		);
		component.find('.events-filter-item').simulate('click');

		expect(store.dispatch).toHaveBeenNthCalledWith(1, removeType('test-event'));
	});
});
