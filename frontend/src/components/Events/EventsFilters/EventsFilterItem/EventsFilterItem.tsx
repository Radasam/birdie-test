import React, { ReactElement } from 'react';
import { useAppDispatch } from '../../../../hooks';
import { eventsIconMapping } from '../../EventsConfig';
import { fetchEvents, removeType, selectType } from '../../EventsSlice';

import './EventsFilterItem.css';

type EventsFilterItemProps = {
	eventType: string;
	selected: Boolean;
};

export const EventsFilterItem = ({
	eventType,
	selected,
}: EventsFilterItemProps): ReactElement => {
	const updateFilters = () => {
		if (selected) {
			dispatch(removeType(eventType));
		} else {
			dispatch(selectType(eventType));
		}
		dispatch(fetchEvents());
	};

	const dispatch = useAppDispatch();
	return (
		<div className="events-filter-item" onClick={() => updateFilters()}>
			<div
				className={
					selected
						? 'filter-item-background --selected'
						: 'filter-item-background'
				}
			>
				<i className={`${eventsIconMapping[eventType as string]}`}></i>
			</div>
		</div>
	);
};
