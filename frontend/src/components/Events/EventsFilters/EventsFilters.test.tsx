import React from 'react';
import { mount } from 'enzyme';
import { EventsFilters } from './EventsFilters';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { EventsFilterItem } from './EventsFilterItem/EventsFilterItem';

describe('EventsFilter', function () {
	it('should render without throwing an error', function () {
		let eventTypes = ['test-1', 'test-2'];
		let component = mount(
			<Provider store={store}>
				<EventsFilters eventTypes={eventTypes} selectedTypes={['test-1']} />
			</Provider>
		);
		expect(component.find(EventsFilterItem)).toHaveLength(eventTypes.length);
	});

	it('should render with no event types', function () {
		let eventTypes: string[] = [];
		let component = mount(
			<Provider store={store}>
				<EventsFilters eventTypes={eventTypes} selectedTypes={['test-1']} />
			</Provider>
		);
		expect(component.find(EventsFilterItem)).toHaveLength(eventTypes.length);
	});
});
