import React, { ReactElement } from 'react';
import { EventsFilterItem } from './EventsFilterItem/EventsFilterItem';

import './EventsFilters.css';

type EventsFiltersProps = {
	eventTypes: string[];
	selectedTypes: String[];
};

export const EventsFilters = ({
	eventTypes,
	selectedTypes,
}: EventsFiltersProps): ReactElement => {
	return (
		<div className="events-filters">
			{eventTypes.map((eventType, index) => (
				<EventsFilterItem
					key={`filter-${eventType}`}
					eventType={eventType}
					selected={selectedTypes.includes(eventType)}
				></EventsFilterItem>
			))}
		</div>
	);
};
