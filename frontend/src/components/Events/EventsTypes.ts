export type EventTypesResponse = {
	event_types: string[];
	start_date: string;
	end_date: string;
};

export type EventType = {
	id: string;
	visit_id: string;
	timestamp: string;
	event_type: string;
	caregiver_id: string;
	care_recipient_id: string;
	care_recipient_name: string;
	caregiver_name: string;
	fluid?: string;
	observed?: boolean;
	consumed_volume_ml?: Number;
	task_instance_id?: string;
	task_schedule_id?: string;
	task_definition_id?: string;
	task_schedule_note?: string;
	task_definition_description?: string;
	note?: string;
	mood?: string;
};

export type EventsIconMapping = {
	[event_type: string]: string;
};

export type EventsResponse = {
	events: Array<EventType>;
	records: number;
	pages: number;
};

export type User = {
	id: string;
	name: string;
};

export type RecipientsResponse = { recipients: User[] };

export type CaregiversResponse = { caregivers: User[] };
