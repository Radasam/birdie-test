import axios from 'axios';
import {
	CaregiversResponse,
	EventsResponse,
	EventTypesResponse,
	RecipientsResponse,
} from './EventsTypes';

export const getEventsRefData = async () => {
	var response = await axios.get<EventTypesResponse>(
		`${process.env.REACT_APP_API_BASE_URL}/events/ref_data`
	);
	return response.data;
};

export const getEvents = async (
	eventTypes: string[],
	selectedCaregiver: string,
	selectedRecipient: string,
	filterMinDate: string,
	filterMaxDate: string,
	currentPage: number,
	rowCount: number
) => {
	var response = await axios.get<EventsResponse>(
		`${process.env.REACT_APP_API_BASE_URL}/events`,
		{
			params: {
				eventTypes,
				selectedCaregiver,
				selectedRecipient,
				startDate: filterMinDate,
				endDate: filterMaxDate,
				pageNumb: currentPage,
				rowNumb: rowCount,
			},
		}
	);
	return response.data;
};

export const getRecipients = async () => {
	var response = await axios.get<RecipientsResponse>(
		`${process.env.REACT_APP_API_BASE_URL}/users/recipients`
	);
	return response.data;
};

export const getCaregivers = async () => {
	var response = await axios.get<CaregiversResponse>(
		`${process.env.REACT_APP_API_BASE_URL}/users/caregivers`
	);
	return response.data;
};
