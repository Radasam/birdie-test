import React, { ReactElement } from 'react';
import { User } from '../EventsTypes';

import './EventsDropDown.css';

type EventsDropDownProps = {
	label: string;
	options: User[];
	onChange: (value: string) => void;
};

export const EventsDropDown = ({
	label,
	options,
	onChange,
}: EventsDropDownProps): ReactElement => {
	return (
		<div className="events-dropdown">
			<h3>{label}</h3>
			<select onChange={(e) => onChange(e.currentTarget.value)}>
				<option value="">All</option>
				{options.map((option) => {
					return (
						<option key={`dropdown-${option.id}`} value={option.id}>
							{option.name}
						</option>
					);
				})}
			</select>
		</div>
	);
};
