import React from 'react';
import { mount } from 'enzyme';
import { EventsDropDown } from './EventsDropDown';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { User } from '../EventsTypes';

describe('EventsDropDown', function () {
	it('should render without throwing an error', function () {
		let label = 'Test Filter:';
		let options: User[] = [
			{ id: 'test-user-1', name: 'test-user-1' },
			{ id: 'test-user-2', name: 'test-user-2' },
		];
		let onChange = jest.fn();
		let component = mount(
			<Provider store={store}>
				<EventsDropDown label={label} options={options} onChange={onChange} />
			</Provider>
		);
		expect(component.find('h3').text()).toEqual(label);
	});

	it('select should trigger onchange', function () {
		let label = 'Test Filter:';
		let options: User[] = [
			{ id: 'test-user-1', name: 'test-user-1' },
			{ id: 'test-user-2', name: 'test-user-2' },
		];
		let onChange = jest.fn();
		let component = mount(
			<Provider store={store}>
				<EventsDropDown label={label} options={options} onChange={onChange} />
			</Provider>
		);
		let select = component.find('select');
		let selectDom = select.getDOMNode() as HTMLInputElement;
		selectDom.value = 'test-user-2';
		select.simulate('change', { currentTarget: selectDom });
		expect(onChange).toHaveBeenCalledWith('test-user-2');
	});
});
