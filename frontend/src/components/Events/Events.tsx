import React, { ReactElement, useEffect } from 'react';
import 'react-dates/lib/css/_datepicker.css';

import 'react-dates/initialize';

import { useAppDispatch } from '../../hooks';
import { EventsDropDown } from './EventsDropDown/EventsDropDown';
import { EventsFilters } from './EventsFilters/EventsFilters';
import { changeCaregiver, changeRecipient, fetchEvents } from './EventsSlice';
import { EventsTable } from './EventsTable/EventsTable';
import { EventType, User } from './EventsTypes';

import './Events.css';
import { EventsDatePicker } from './EventsDatePicker/EventsDatePicker';
import { EventsPagination } from './EventsPagination/EventsPagination';

type EventsProps = {
	eventTypes: string[];
	selectedTypes: String[];
	events: EventType[];
	caregivers: User[];
	recipients: User[];
	filterMinDate: string;
	filterMaxDate: string;
	refDataLoaded: boolean;
	eventsLoaded: boolean;
	currentPage: number;
	totalPages: number;
	rowCount: number;
	totalRecords: number;
};

export const Events = ({
	eventTypes,
	selectedTypes,
	events,
	caregivers,
	recipients,
	filterMinDate,
	filterMaxDate,
	refDataLoaded,
	eventsLoaded,
	currentPage,
	totalPages,
	rowCount,
	totalRecords,
}: EventsProps): ReactElement => {
	const dispatch = useAppDispatch();

	const handleChangeCaregiver = (value: string) => {
		dispatch(changeCaregiver(value));
		dispatch(fetchEvents());
	};

	const handleChangeRecipient = (value: string) => {
		dispatch(changeRecipient(value));
		dispatch(fetchEvents());
	};

	useEffect(() => {
		dispatch(fetchEvents());
	}, [refDataLoaded, dispatch]);

	return (
		<div className="events">
			<div className="events-header">
				<h1>Events</h1>
				<div className="events-header-filters">
					<EventsDatePicker
						filterMinDate={filterMinDate}
						filterMaxDate={filterMaxDate}
					></EventsDatePicker>
					<EventsDropDown
						label={'Recipients :'}
						options={recipients}
						onChange={handleChangeRecipient}
					></EventsDropDown>
					<EventsDropDown
						label={'Caregiver :'}
						options={caregivers}
						onChange={handleChangeCaregiver}
					></EventsDropDown>
				</div>
			</div>
			<div className="events-content">
				<EventsFilters
					eventTypes={eventTypes}
					selectedTypes={selectedTypes}
				></EventsFilters>
				<div className="events-content-table">
					<EventsPagination
						totalPages={totalPages}
						totalRecords={totalRecords}
						currentPage={currentPage}
						rowCount={rowCount}
					></EventsPagination>
					<EventsTable
						events={events}
						eventsLoaded={eventsLoaded}
					></EventsTable>
				</div>
			</div>
		</div>
	);
};
