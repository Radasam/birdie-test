import reducer, {
	changeCaregiver,
	changeDates,
	changePage,
	changeRecipient,
	changeRowCount,
	fetchCaregivers,
	fetchEvents,
	fetchEventsRefData,
	fetchRecipients,
	initialState,
	removeType,
	selectType,
} from './EventsSlice';
import {
	CaregiversResponse,
	EventsResponse,
	EventTypesResponse,
	RecipientsResponse,
} from './EventsTypes';

describe('EventsSlice', function () {
	it('should return the initial state', () => {
		expect(reducer(undefined, { type: '' })).toEqual(initialState);
	});

	it('add event to selected types', () => {
		expect(
			reducer(initialState, selectType('test-event')).selectedTypes
		).toEqual(['test-event']);
	});

	it('remove event from selected types', () => {
		expect(
			reducer(
				{ ...initialState, selectedTypes: ['test-event-1', 'test-event-2'] },
				removeType('test-event-1')
			).selectedTypes
		).toEqual(['test-event-2']);
	});

	it('change caregiver', () => {
		expect(
			reducer(initialState, changeCaregiver('test-caregiver')).selectedCareGiver
		).toEqual('test-caregiver');
	});

	it('change recipient', () => {
		expect(
			reducer(initialState, changeRecipient('test-recipient')).selectedRecipient
		).toEqual('test-recipient');
	});

	it('change date', () => {
		let eventsReducer = reducer(
			initialState,
			changeDates({
				startDate: '2021-01-01T00:00:00.000Z',
				endDate: '2021-01-02T00:00:00.000Z',
			})
		);
		expect([eventsReducer.filterMinDate, eventsReducer.filterMaxDate]).toEqual([
			'2021-01-01T00:00:00.000Z',
			'2021-01-02T00:00:00.000Z',
		]);
	});

	it('change page', () => {
		expect(reducer(initialState, changePage(2)).currentPage).toEqual(2);
	});

	it('change row count', () => {
		expect(reducer(initialState, changeRowCount(50)).rowCount).toEqual(50);
	});

	it('fetch event_types succeeds', async () => {
		let returnedRefData: EventTypesResponse = {
			event_types: ['test-type-1', 'test-type-2'],
			start_date: '2021-01-01T00:00:00.000Z',
			end_date: '2021-01-02T00:00:00.000Z',
		};

		const action = fetchEventsRefData.fulfilled(returnedRefData, '');

		let eventsReducer = reducer(initialState, action);

		expect(eventsReducer.eventTypes).toEqual(returnedRefData.event_types);
		expect(eventsReducer.filterMinDate).toEqual(returnedRefData.start_date);
		expect(eventsReducer.filterMaxDate).toEqual(returnedRefData.end_date);
		expect(eventsReducer.refDataLoaded).toBeTruthy();
	});

	it('fetch event_types pending', async () => {
		const action = fetchEventsRefData.pending('');

		let eventsReducer = reducer(initialState, action);

		expect(eventsReducer.refDataLoaded).toBeFalsy();
	});

	it('fetch events succeeds', async () => {
		let returnedEvents: EventsResponse = {
			events: [
				{
					id: 'test-1',
					visit_id: 'visit-1',
					timestamp: '2021-07-23T00:00:00.000Z',
					event_type: 'test-event',
					caregiver_id: 'caregiver-1',
					care_recipient_id: 'recipient-1',
					caregiver_name: 'test cargiver',
					care_recipient_name: 'test recipient',
				},
				{
					id: 'test-2',
					visit_id: 'visit-1',
					timestamp: '2021-07-23T00:00:00.000Z',
					event_type: 'test-event',
					caregiver_id: 'caregiver-1',
					care_recipient_id: 'recipient-2',
					caregiver_name: 'test cargiver',
					care_recipient_name: 'test recipient',
				},
			],
			pages: 2,
			records: 50,
		};

		const action = fetchEvents.fulfilled(returnedEvents, '');

		let eventsReducer = reducer(initialState, action);

		expect(eventsReducer.events).toEqual(returnedEvents.events);
		expect(eventsReducer.totalPages).toEqual(returnedEvents.pages);
		expect(eventsReducer.totalRecords).toEqual(returnedEvents.records);
		expect(eventsReducer.eventsLoaded).toBeTruthy();
	});

	it('fetch events pending', async () => {
		const action = fetchEvents.pending('');

		let eventsReducer = reducer(initialState, action);

		expect(eventsReducer.eventsLoaded).toBeFalsy();
	});

	it('fetch caregivers succeeds', async () => {
		let returnedCaregivers: CaregiversResponse = {
			caregivers: [
				{ id: 'test-caregiver-1', name: 'test caregiver' },
				{ id: 'test-caregiver-2', name: 'test caregiver' },
			],
		};

		const action = fetchCaregivers.fulfilled(returnedCaregivers, '');

		let eventsReducer = reducer(initialState, action);

		expect(eventsReducer.caregivers).toEqual(returnedCaregivers.caregivers);
	});

	it('fetch recipients succeeds', async () => {
		let returnedRecipients: RecipientsResponse = {
			recipients: [
				{ id: 'test-recipients-1', name: 'test recipients' },
				{ id: 'test-recipients-2', name: 'test recipients' },
			],
		};

		const action = fetchRecipients.fulfilled(returnedRecipients, '');

		let eventsReducer = reducer(initialState, action);

		expect(eventsReducer.recipients).toEqual(returnedRecipients.recipients);
	});
});
