import React, { ReactElement } from 'react';
import { EventType } from '../EventsTypes';
import { EventsTableRow } from './EventsTableRow/EventsTableRow';

import './EventsTable.css';
import { EventsTableRowPlaceholder } from './EventsTableRowPlaceholder/EventsTableRowPlaceholder';

type EventsTableProps = {
	events: Array<EventType>;
	eventsLoaded: boolean;
};

export const EventsTable = ({
	events,
	eventsLoaded,
}: EventsTableProps): ReactElement => {
	return (
		<div className="events-table">
			{eventsLoaded ? (
				<>
					{events.map((event) => (
						<EventsTableRow
							key={`event-${event.id}`}
							event={event}
						></EventsTableRow>
					))}
				</>
			) : (
				<>
					{Array.from(new Array(10).keys()).map((item) => (
						<EventsTableRowPlaceholder
							key={`row-placeholder-${item}`}
						></EventsTableRowPlaceholder>
					))}
				</>
			)}
		</div>
	);
};
