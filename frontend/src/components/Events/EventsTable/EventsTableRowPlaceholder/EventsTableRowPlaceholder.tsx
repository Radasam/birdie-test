import React, { ReactElement } from 'react';

import './EventsTableRowPlaceholder.css';

export const EventsTableRowPlaceholder = (): ReactElement => {
	return (
		<div className="events-table-row">
			<div className="events-table-row-icon blinking"></div>
			<div className="events-table-row-content">
				<div className="event-text">
					<div className="event-text-header">
						<h3
							className="event-text-header-item text-placeholder"
							style={{ width: '180px', height: '1.2rem' }}
						></h3>
						<h3
							className="event-text-header-item text-placeholder"
							style={{ width: '180px', height: '1.2rem' }}
						></h3>
					</div>
					<div className="event-text-items">
						<div className="event-text-item">
							<p
								className="event-text-header-item text-placeholder"
								style={{ width: '100px', height: '1.2rem' }}
							></p>
						</div>
						<div className="event-text-item">
							<p
								className="event-text-header-item text-placeholder"
								style={{ width: '100px', height: '1.2rem' }}
							></p>
						</div>
					</div>
				</div>
				<div className="event-date">
					<div
						className="event-text-header-item text-placeholder"
						style={{ width: '30px', height: '1.2rem' }}
					></div>
				</div>
			</div>
		</div>
	);
};
