import React, { ReactElement } from 'react';
import formatDate from '../../../../helpers/formatDate';
import formatDateTime from '../../../../helpers/formatDateTime';
import formatType from '../../../../helpers/formatType';
import { eventsIconMapping } from '../../EventsConfig';
import { EventType } from '../../EventsTypes';

import './EventsTableRow.css';

type EventsTableRowProps = {
	event: EventType;
};

export const EventsTableRow = ({
	event,
}: EventsTableRowProps): ReactElement => {
	return (
		<div className="events-table-row">
			<div className="events-table-row-icon">
				<i className={eventsIconMapping[event.event_type]}></i>
			</div>
			<div className="events-table-row-content">
				<div className="event-text">
					<div className="event-text-header">
						<h3 className="event-text-header-item">
							{formatDateTime(event.timestamp)}
						</h3>
						<h3 className="event-text-header-item">
							{formatType(event.event_type as string)}
						</h3>
					</div>
					<div className="event-text-items">
						<div className="event-text-item">
							<p className="event-text-label">Recipient:</p>
							<p className="event-text-value">{event.care_recipient_name}</p>
						</div>
						{event.caregiver_id && (
							<div className="event-text-item">
								<p className="event-text-label">Caregiver:</p>
								<p className="event-text-value">{event.caregiver_name}</p>
							</div>
						)}
					</div>
					{event.mood && (
						<div className="event-text-items">
							<div className="event-text-item">
								<p className="event-text-label">Mood:</p>
								<p className="event-text-value">{event.mood}</p>
							</div>
						</div>
					)}
					{event.task_definition_description && (
						<div className="event-text-desc">
							{event.task_definition_description}
						</div>
					)}
					{event.note && <div className="event-text-desc">{event.note}</div>}
				</div>
				<div className="event-date">{formatDate(event.timestamp)}</div>
			</div>
		</div>
	);
};
