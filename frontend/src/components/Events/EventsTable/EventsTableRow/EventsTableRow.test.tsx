import React from 'react';
import { mount } from 'enzyme';
import { EventsTableRow } from './EventsTableRow';
import { Provider } from 'react-redux';
import { store } from '../../../../store';
import { EventType } from '../../EventsTypes';
import formatDateTime from '../../../../helpers/formatDateTime';
import formatType from '../../../../helpers/formatType';
import { eventsIconMapping } from '../../EventsConfig';

describe('EventsTableRow', function () {
	it('should render without throwing an error', function () {
		let event: EventType = {
			id: 'abc-1',
			visit_id: 'def-2',
			timestamp: '2021-07-23T00:00:00.000Z',
			event_type: 'fluid_intake_observation',
			caregiver_id: 'ghi-3',
			care_recipient_id: 'jkl-4',
			caregiver_name: 'test-caregiver',
			care_recipient_name: 'test-recipient',
		};
		let component = mount(
			<Provider store={store}>
				<EventsTableRow event={event} />
			</Provider>
		);
		expect(component.find('.event-text-header-item').at(0).text()).toEqual(
			formatDateTime(event.timestamp)
		);
		expect(component.find('.event-text-header-item').at(1).text()).toEqual(
			formatType(event.event_type)
		);
		expect(component.find('.event-text-value').at(0).text()).toEqual(
			event.care_recipient_name
		);
		expect(component.find('.event-text-value').at(1).text()).toEqual(
			event.caregiver_name
		);
		expect(component.find('i').hasClass(eventsIconMapping[event.event_type]));
		expect(component.find('.event-text-desc')).toHaveLength(0);
	});

	it('should render description', function () {
		let event: EventType = {
			id: 'abc-1',
			visit_id: 'def-2',
			timestamp: '2021-07-23T00:00:00.000Z',
			event_type: 'fluid_intake_observation',
			caregiver_id: 'ghi-3',
			care_recipient_id: 'jkl-4',
			caregiver_name: 'test-caregiver',
			care_recipient_name: 'test-recipient',
			task_definition_description: 'test-desc',
		};
		let component = mount(
			<Provider store={store}>
				<EventsTableRow event={event} />
			</Provider>
		);
		expect(component.find('.event-text-desc').text()).toEqual(
			event.task_definition_description
		);
	});
});
