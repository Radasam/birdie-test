import React from 'react';
import { mount } from 'enzyme';
import { EventsTable } from './EventsTable';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { EventType } from '../EventsTypes';
import { EventsTableRow } from './EventsTableRow/EventsTableRow';
import { EventsTableRowPlaceholder } from './EventsTableRowPlaceholder/EventsTableRowPlaceholder';

describe('EventsTable', function () {
	it('should render without throwing an error', function () {
		let events: EventType[] = [
			{
				id: 'abc-1',
				visit_id: 'def-1',
				timestamp: '2021-07-23T00:00:00.000Z',
				event_type: 'fluid_intake_observation',
				caregiver_id: 'ghi-1',
				care_recipient_id: 'jkl-1',
				caregiver_name: 'test-caregiver',
				care_recipient_name: 'test-recipient',
			},
			{
				id: 'abc-2',
				visit_id: 'def-2',
				timestamp: '2021-07-20T00:00:00.000Z',
				event_type: 'fluid_intake_observation',
				caregiver_id: 'ghi-2',
				care_recipient_id: 'jkl-2',
				caregiver_name: 'test-caregiver-2',
				care_recipient_name: 'test-recipient-2',
				task_definition_description: 'test-desc',
			},
		];
		let component = mount(
			<Provider store={store}>
				<EventsTable events={events} eventsLoaded={true} />
			</Provider>
		);
		expect(component.find(EventsTableRow)).toHaveLength(events.length);
	});

	it('should render with no events', function () {
		let events: EventType[] = [];
		let component = mount(
			<Provider store={store}>
				<EventsTable events={events} eventsLoaded={true} />
			</Provider>
		);
		expect(component.find(EventsTableRow)).toHaveLength(events.length);
	});

	it('should render placeholders when events are loading', function () {
		let events: EventType[] = [];
		let component = mount(
			<Provider store={store}>
				<EventsTable events={events} eventsLoaded={false} />
			</Provider>
		);
		expect(component.find(EventsTableRowPlaceholder)).toHaveLength(10);
	});
});
