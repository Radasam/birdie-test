import { EventsIconMapping } from './EventsTypes';

export const eventsIconMapping: EventsIconMapping = {
	fluid_intake_observation: 'fas fa-tint',
	task_completed: 'far fa-check-circle',
	physical_health_observation: 'fas fa-notes-medical',
	visit_completed: 'fas fa-house-user',
	check_out: 'far fa-id-badge',
	mood_observation: 'fas fa-smile',
	regular_medication_taken: 'far fa-calendar-check',
	alert_raised: 'far fa-bell',
	no_medication_observation_received: 'far fa-eye-slash',
	incontinence_pad_observation: 'far fa-eye',
	check_in: 'fas fa-id-badge',
	general_observation: 'fas fa-user-friends',
	regular_medication_not_taken: 'far fa-calendar-times',
	food_intake_observation: 'fas fa-utensils',
	task_completion_reverted: 'fas fa-undo',
	mental_health_observation: 'fas fa-brain',
	medication_schedule_updated: 'far fa-calendar-plus',
	visit_cancelled: 'fas fa-ban',
	regular_medication_maybe_taken: 'far fa-question-circle',
	medication_schedule_created: 'far fa-calendar',
	alert_qualified: 'fas fa-bell',
	task_schedule_created: 'fas fa-tasks',
	concern_raised: 'fas fa-exclamation-triangle',
	regular_medication_partially_taken: 'far fa-calendar-minus',
	catheter_observation: 'fas fa-grip-lines',
	toilet_visit_recorded: 'fas fa-toilet',
};
