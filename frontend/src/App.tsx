import React from 'react';
import './App.css';
import { Events } from './components/Events/Events';
import { useAppSelector } from './hooks';

function App() {
	const {
		eventTypes,
		selectedTypes,
		events,
		caregivers,
		recipients,
		filterMinDate,
		filterMaxDate,
		refDataLoaded,
		eventsLoaded,
		currentPage,
		totalPages,
		totalRecords,
		rowCount,
	} = useAppSelector((state) => state.events);

	return (
		<div className="app">
			{refDataLoaded && (
				<Events
					eventTypes={eventTypes}
					selectedTypes={selectedTypes}
					events={events}
					caregivers={caregivers}
					recipients={recipients}
					filterMinDate={filterMinDate}
					filterMaxDate={filterMaxDate}
					refDataLoaded={refDataLoaded}
					eventsLoaded={eventsLoaded}
					currentPage={currentPage}
					totalPages={totalPages}
					rowCount={rowCount}
					totalRecords={totalRecords}
				></Events>
			)}
		</div>
	);
}

export default App;
