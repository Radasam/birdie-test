import capitalise from './capitalise';

const formatType = (eventType: string) => {
	return eventType
		.split('_')
		.map((subString: string) => capitalise(subString))
		.join(' ');
};

export default formatType;
