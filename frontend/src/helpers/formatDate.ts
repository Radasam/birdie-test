const formatDate = (timestamp: String) => {
	let date = new Date(timestamp as string);
	let currDate = new Date();
	let dateOut = '';
	if (
		currDate.getFullYear() - date.getFullYear() > 0 ||
		currDate.getMonth() - date.getMonth() > 0
	) {
		dateOut =
			Math.round(
				(currDate.getTime() - date.getTime()) / (7 * 24 * 60 * 60 * 1000)
			) + 'w';
	} else {
		dateOut =
			Math.floor(
				(currDate.getTime() - date.getTime()) / (7 * 24 * 60 * 60 * 1000)
			) +
			'w ' +
			Math.round(
				(currDate.getTime() - date.getTime()) / (24 * 60 * 60 * 1000)
			) +
			'd';
	}

	return dateOut;
};

export default formatDate;
