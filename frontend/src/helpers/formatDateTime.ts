const formatDateTime = (timeStamp: string) => {
	let date = new Date(timeStamp);
	return `${date.toLocaleTimeString()} ${date.toLocaleDateString()}`;
};

export default formatDateTime;
