export default {
	moduleNameMapper: {
		'\\.(css|less)$': '<rootDir>/styleMock.ts',
	},
};
