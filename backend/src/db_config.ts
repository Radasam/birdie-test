import { createConnection } from 'mysql2'
import { load } from 'ts-dotenv'

const env = load({
    DB_HOST: String,
    DB_PORT: Number,
    DB_USER: String,
    DB_PASS: String,
    DB_DATABASE: String,
})

export default createConnection({
    host: env.DB_HOST,
    port: env.DB_PORT,
    user: env.DB_USER,
    password: env.DB_PASS,
    database: env.DB_DATABASE,
})
