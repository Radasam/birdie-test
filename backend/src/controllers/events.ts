import * as express from 'express'
import { RowDataPacket } from 'mysql2'
import connection from '../db_config'
import { careRecipientMap, careGiverMap } from '../dummy'

export const eventsController = express.Router()

eventsController.get('/', async (req, res) => {
    let eventTypes = req.query.eventTypes
    let selectedCaregiver = req.query.selectedCaregiver
    let selectedRecipient = req.query.selectedRecipient
    let startDate = req.query.startDate
    let endDate = req.query.endDate
    let rowNumb = req.query.rowNumb
    let pageNumb = req.query.pageNumb
    let startingRow = ''

    let allEventTypes: number = 1
    let allCaregivers: number = 1
    let allRecipients: number = 1
    let array_placeholder: string[] = ['?']

    if (rowNumb && pageNumb) {
        startingRow = (
            parseInt(rowNumb as string, 10) * parseInt(pageNumb as string, 10)
        ).toString()
    }

    if (Array.isArray(eventTypes)) {
        array_placeholder = eventTypes.map((_) => '?')
    } else if (eventTypes) {
        eventTypes = [eventTypes] as string[]
    } else {
        allEventTypes = 0
        eventTypes = ['']
    }

    if (selectedCaregiver === '') {
        allCaregivers = 0
    }

    if (selectedRecipient === '') {
        allRecipients = 0
    }
    try {
        connection.execute(
            `SELECT payload, records, pages
        FROM events
        cross join
        (
        select count(1) as records
        ,CEILING(count(1)/?) as pages 
        from birdietest.events e
        where (0 = ? or event_type in (${array_placeholder.join(',')}))
        and (0 = ? or caregiver_id = ?)
        and (0 = ? or care_recipient_id  = ?)
        and \`timestamp\` between ? and ?
        order by \`timestamp\` DESC 
        ) c
        where (0 = ? or event_type in (${array_placeholder.join(',')}))
        and (0 = ? or caregiver_id = ?)
        and (0 = ? or care_recipient_id  = ?)
        and \`timestamp\` between ? and ?
        order by \`timestamp\` DESC 
        limit ?,?`,
            [
                rowNumb,
                allEventTypes,
                ...eventTypes,
                allCaregivers,
                selectedCaregiver,
                allRecipients,
                selectedRecipient,
                startDate,
                endDate,
                allEventTypes,
                ...eventTypes,
                allCaregivers,
                selectedCaregiver,
                allRecipients,
                selectedRecipient,
                startDate,
                endDate,
                startingRow,
                rowNumb,
            ],
            (_error, results: RowDataPacket[], _fields) => {
                if (_error) {
                    res.status(500).json({
                        message: 'There was an error processing your request.',
                    })
                }
                if (results && results.length > 0) {
                    res.status(200).json({
                        events: results.map((resultItem: RowDataPacket) => {
                            let event = resultItem.payload
                            event['care_recipient_name'] =
                                careRecipientMap[event['care_recipient_id']]
                            event['caregiver_name'] =
                                careGiverMap[event['caregiver_id']]
                            return event
                        }),
                        records: results[0].records,
                        pages: results[0].pages,
                    })
                } else {
                    res.status(200).json({
                        events: [],
                        records: 0,
                        pages: 1,
                    })
                }
            }
        )
    } catch {
        res.status(500).json({
            message: 'There was an error processing your request.',
        })
    }
})

eventsController.get('/ref_data', (_, res) => {
    try {
        connection.execute(
            `select distinct event_type from events`,
            (_error, typeResults: RowDataPacket[], _fields) => {
                if (_error) {
                    res.status(500).json({
                        message: 'There was an error processing your request.',
                    })
                }
                connection.execute(
                    `SELECT DATE_FORMAT(min(\`timestamp\`),'%Y-%m-%dT%H:%i:%s.000Z') as start_date, DATE_FORMAT(max(\`timestamp\`),'%Y-%m-%dT%H:%i:%s.000Z') as end_date from birdietest.events e `,
                    (_error, dateResults: RowDataPacket[], _fields) => {
                        res.status(200).json({
                            event_types: typeResults.map(
                                (resultItem: RowDataPacket) => {
                                    return resultItem.event_type
                                }
                            ),
                            start_date: dateResults[0].start_date,
                            end_date: dateResults[0].end_date,
                        })
                    }
                )
            }
        )
    } catch {
        res.status(500).json({
            message: 'There was an error processing your request.',
        })
    }
})
