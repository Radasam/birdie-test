import * as express from 'express'

import { careRecipientMap, careGiverMap } from '../dummy'

export const usersController = express.Router()

usersController.get('/recipients', async (_, res) => {
    const recipients = Object.keys(careRecipientMap).map((key: string) => {
        return { id: key, name: careRecipientMap[key] }
    })
    res.status(200).json({
        recipients,
    })
})

usersController.get('/caregivers', async (_, res) => {
    const caregivers = Object.keys(careGiverMap).map((key: string) => {
        return { id: key, name: careGiverMap[key] }
    })
    res.status(200).json({
        caregivers,
    })
})
