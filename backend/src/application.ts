import express = require('express')
import { pingController } from './controllers/ping'
import { eventsController } from './controllers/events'
import { usersController } from './controllers/users'
import cors = require('cors')

const app = express()

app.use(cors())

app.use(pingController)
app.use('/events', eventsController)
app.use('/users', usersController)

export default app
