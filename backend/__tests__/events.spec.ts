import app from '../src/application'
import request from 'supertest'

// I use snapshots for tests here, In a real environment these would be tested against some static dummy test data
// I will also avoid pushing these snapshots to git as it is not my data, but in reality the snapshots would sit on the code repo
describe('Events', () => {
    it('all events', async () => {
        await request(app)
            .get(
                '/events?eventTypes=&startDate=2019-04-23T02:00:11.272Z&endDate=2019-05-12T22:06:34.272Z&selectedCaregiver=&selectedRecipient&rowNumb=25&pageNumb=0'
            )
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })

    it('single event type filter', async () => {
        await request(app)
            .get(
                '/events?eventTypes=visit_completed&startDate=2019-04-23T02:00:11.272Z&endDate=2019-05-12T22:06:34.272Z&selectedRecipient&selectedCaregiver=&rowNumb=25&pageNumb=0'
            )
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })

    it('multiple event type filter', async () => {
        await request(app)
            .get(
                '/events?eventTypes=task_completed&eventTypes=visit_completed&startDate=2019-04-23T02:00:11.272Z&endDate=2019-05-12T22:06:34.272Z&selectedRecipient&selectedCaregiver=&rowNumb=25&pageNumb=0'
            )
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })

    it('caregiver filter', async () => {
        await request(app)
            .get(
                '/events?startDate=2019-04-23T02:00:11.272Z&endDate=2019-05-12T22:06:34.272Z&selectedRecipient&selectedCaregiver=019984a8-c36c-430a-baf6-d668a790ef4f&rowNumb=25&pageNumb=0'
            )
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })

    it('recipient filter', async () => {
        await request(app)
            .get(
                '/events?startDate=2019-04-23T02:00:11.272Z&endDate=2019-05-12T22:06:34.272Z&selectedRecipient=df50cac5-293c-490d-a06c-ee26796f850d&selectedCaregiver=&rowNumb=25&pageNumb=0'
            )
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })

    it('date filter', async () => {
        await request(app)
            .get(
                '/events?startDate=2019-04-26T13:00:00.000Z&endDate=2019-04-27T12:00:00.000Z&selectedRecipient=&selectedCaregiver=&rowNumb=25&pageNumb=0'
            )
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })

    it('change page', async () => {
        await request(app)
            .get(
                '/events?startDate=2019-04-26T13:00:00.000Z&endDate=2019-04-27T12:00:00.000Z&selectedRecipient=&selectedCaregiver=&rowNumb=25&pageNumb=1'
            )
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })

    it('change row count', async () => {
        await request(app)
            .get(
                '/events?startDate=2019-04-26T13:00:00.000Z&endDate=2019-04-27T12:00:00.000Z&selectedRecipient=&selectedCaregiver=&rowNumb=50&pageNumb=0'
            )
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })
})

describe('ref data', () => {
    it('ref_data', async () => {
        await request(app)
            .get('/events/ref_data')
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })
})
