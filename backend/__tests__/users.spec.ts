import app from '../src/application'
import request from 'supertest'

// I use snapshots for tests here, In a real environment these would be tested against some static dummy test data
// I will also avoid pushing these snapshots to git as it is not my data, but in reality the snapshots would sit on the code repo

describe('users', () => {
    it('caregivers', async () => {
        await request(app)
            .get('/users/caregivers')
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })

    it('recipients', async () => {
        await request(app)
            .get('/users/recipients')
            .expect(200)
            .expect(function (res) {
                expect(res.body).toMatchSnapshot()
            })
    })
})
